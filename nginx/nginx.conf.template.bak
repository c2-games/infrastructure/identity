server
{
	listen 80 http2;
	server_name _;

	location /.well-known/acme-challenge/
	{
		proxy_pass http://acme;
	}
}

server
{
	listen 443 ssl http2;
	server_name _;
	ssl_certificate /etc/certs/nginx/fullchain;
	ssl_certificate_key /etc/certs/nginx/key;
	proxy_buffer_size 128k;
	proxy_buffers 4 256k;
	proxy_busy_buffers_size 256k;

	location /
	{
		if ($http_origin = '')
		{
			set $http_origin "*";
		}
		if ($request_method = 'OPTIONS')
		{
			add_header 'Access-Control-Allow-Origin' $http_origin;
			add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
			#
			# Custom headers and headers various browsers *should* be OK with but aren't
			#
			add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
			#
			# Tell client that this pre-flight info is valid for 20 days
			#
			add_header 'Access-Control-Max-Age' 1728000;
			add_header 'Content-Type' 'text/plain; charset=utf-8';
			add_header 'Content-Length' 0;
			return 204;
		}
		if ($request_method = 'POST')
		{
			add_header 'Access-Control-Allow-Origin' '*' always;
			add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
			add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
			add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
			add_header 'Access-Control-Allow-Credentials' true always;
		}
		if ($request_method = 'GET')
		{
			add_header 'Access-Control-Allow-Origin' $http_origin always;
			add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
			add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
			add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
		}

		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_pass http://keycloak:8080;
	}
}
